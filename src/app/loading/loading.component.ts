import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent implements OnInit {
  isshown = false;
  constructor(private router: Router) { }

  ngOnInit() {
    setTimeout(function () {
      document.getElementById("myDiv").style.display = "none";
    }, 10000);
    this.router.navigate(['forminfo'])

  }

}
