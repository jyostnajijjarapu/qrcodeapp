import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  qrData = null;
  createdCode = null;
  scannedCode = null;
  constructor(private barcodeScanner: BarcodeScanner, private router: Router, private common: CommonService) { }

  createCode() {
    this.createdCode = this.qrData;
    this.common.eqpName = this.createdCode;
  }
  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    })
    this.router.navigate(['/loading']);
  }
  logout() {
    this.router.navigate(['/login'])
  }
  navigateTOReport() {
    this.router.navigate(['/report'])
  }
}
