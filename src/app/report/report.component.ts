import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {

  constructor(public common: CommonService, private router: Router) { }

  ngOnInit() {

  }
  generateReport() {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.common.formReport);

    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, "reportdata.xlsx")
  }
  qrcodescan() {
    this.router.navigate(['/home'])
  }
  logoutHandler() {
    this.router.navigate(['/login'])
  }
}
