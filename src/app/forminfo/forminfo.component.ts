import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-forminfo',
  templateUrl: './forminfo.component.html',
  styleUrls: ['./forminfo.component.scss'],
})
export class ForminfoComponent implements OnInit {
  @ViewChild('slideForm1') slideForm1: IonSlides;
  user: any = {};

  constructor(public common: CommonService, private router: Router) { }

  ngOnInit() {

    this.user.eqpName = this.common.eqpName
  }
  next() {

    this.slideForm1.slideNext();
  }



  prev() {
    this.slideForm1.slidePrev();
  }

  submitreport() {
    this.common.formReport.push(this.user);

    this.router.navigate(['/report'])



  }
  navigateTOReport() {
    this.router.navigate(['/report'])
  }
  qrcodescan() {
    this.router.navigate(['/home'])
  }
  logout() {
    this.router.navigate(['/login'])

  }
}
